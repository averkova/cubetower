using UnityEngine;

public class RotateCamera : MonoBehaviour
{
    [SerializeField]
    private float speed = 5.0F;

    private Transform _rotator;

    private void Start()
    {
        _rotator = GetComponent<Transform>();
    }

    private void Update()
    {
        _rotator.Rotate(0, speed * Time.deltaTime, 0);
    }
}
