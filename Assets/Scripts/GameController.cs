using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    private CubePos nowCube = new CubePos(0, 1, 0);  

    private float cubeChangePlaceSpeed = 0.5F;

    private float camMoveToYPosition;
    private Transform mainCamera;

    private Rigidbody allCubesRb;

    private bool isLose, firstCube;

    [SerializeField]
    private float cameraSpeed = 2.0F;

    [SerializeField]
    private Transform cubeToPlace;

    [SerializeField]
    private TMP_Text scoreTxt;

    [SerializeField]
    private GameObject allCubes, vfx;

    [SerializeField]
    private GameObject[] cubesToCreate;

    [SerializeField]
    private GameObject[] canvasStartPage;
    
    [SerializeField]
    private Color[] bgColor; 

    private Color toCameraColor;

    private List<GameObject> possibleCubesToCreate = new List<GameObject>();

    private List<Vector3> allCubesPositions = new List<Vector3>
    {
        new Vector3(0,0,0),
        new Vector3(1,0,0),
        new Vector3(-1,0,0),
        new Vector3(0,1,0),
        new Vector3(0,0,1),
        new Vector3(0,0,-1),
        new Vector3(1,0,1),
        new Vector3(-1,0,-1),
        new Vector3(-1,0,1),
        new Vector3(1,0,-1),
    };

    private int prevCountMaxHorizontal;

    private Coroutine showCubePlace;

    private bool _IsEscape = false;

    private void Start()
    {
        if (PlayerPrefs.GetInt("score") < 1) possibleCubesToCreate.Add(cubesToCreate[0]);
        else if (PlayerPrefs.GetInt("score") < 3) AddPossibleCubes(2);
        else if (PlayerPrefs.GetInt("score") < 6) AddPossibleCubes(3);
        else if (PlayerPrefs.GetInt("score") < 12) AddPossibleCubes(4);
        else if (PlayerPrefs.GetInt("score") < 24) AddPossibleCubes(5);
        else if (PlayerPrefs.GetInt("score") < 48) AddPossibleCubes(6);
        else if (PlayerPrefs.GetInt("score") < 55) AddPossibleCubes(7);
        else if (PlayerPrefs.GetInt("score") < 70) AddPossibleCubes(8);
        else if (PlayerPrefs.GetInt("score") < 88) AddPossibleCubes(9);
        else AddPossibleCubes(10);

        scoreTxt.text = $"<size=310><color=#A22521> best: </color></size> {PlayerPrefs.GetInt("score")} \n<size=290> now: </size> 0";

        mainCamera = Camera.main.transform;
        camMoveToYPosition = 5.9F + nowCube.y - 1.0F;

        toCameraColor = Camera.main.backgroundColor;

        showCubePlace = StartCoroutine(ShowCubePlace());
        allCubesRb = allCubes.GetComponent<Rigidbody>();
    }

    private void Update()
    {
       // if (SceneManager.GetActiveScene().name == "main" && Input.GetKeyUp(KeyCode.Escape)) Application.Quit();

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (_IsEscape)
            {
                Application.Quit();
            }
            else
            {
                _IsEscape = true;
                if (!IsInvoking("DisableDoubleClick"))
                    Invoke("DisableDoubleClick", 0.3f);
            }
        }


        if ((Input.GetMouseButtonDown(0) || Input.touchCount > 0) && cubeToPlace != null && allCubes != null)
                {
#if !UNITY_EDITOR
                        if (Input.GetTouch(0).phase != TouchPhase.Began || EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) return;
                        
#endif
            //if () return;
            if (!firstCube)
                    {
                        firstCube = true;
                        foreach (GameObject obj in canvasStartPage)
                        {
                            Destroy(obj);
                        }
                    }

                    GameObject createCube = null;

                    if (possibleCubesToCreate.Count == 1)
                        createCube = possibleCubesToCreate[0];
                    else
                        createCube = possibleCubesToCreate[UnityEngine.Random.Range(0, possibleCubesToCreate.Count)];

                    GameObject newCube = Instantiate(createCube, cubeToPlace.position, Quaternion.identity);

                    newCube.transform.SetParent(allCubes.transform);

                    nowCube.setVector(cubeToPlace.position);
                    allCubesPositions.Add(nowCube.getVector());

                    if (PlayerPrefs.GetString("music") != "No")
                        GetComponent<AudioSource>().Play();

                    GameObject newVfx = Instantiate(vfx, cubeToPlace.position, Quaternion.identity);
                    Destroy(newVfx, 2.0F);

                    allCubesRb.isKinematic = true;
                    allCubesRb.isKinematic = false;

                    SpawnPosition();

                    MoveCameraChangeBg();

            
                }
                if (!isLose && allCubesRb.velocity.magnitude > 0.1F)
                {
                    Destroy(cubeToPlace.gameObject);
                    isLose = true;
                    StopCoroutine(showCubePlace);
                }



                mainCamera.localPosition = Vector3.MoveTowards(mainCamera.localPosition, new Vector3(mainCamera.localPosition.x, camMoveToYPosition, mainCamera.localPosition.z), cameraSpeed*Time.deltaTime);

                if (Camera.main.backgroundColor != toCameraColor) Camera.main.backgroundColor = Color.Lerp(Camera.main.backgroundColor, toCameraColor, Time.deltaTime / 2.0F);
            }

    void DisableDoubleClick()
    {
        _IsEscape = false;
    }

    IEnumerator ShowCubePlace()
    {
        while (true)
        {
            SpawnPosition();
            yield return new WaitForSeconds(cubeChangePlaceSpeed);
        }
    }

    private void SpawnPosition()
    {
        List<Vector3> positions = new List<Vector3>();
        if (IsPositionEmpty(new Vector3(nowCube.x + 1, nowCube.y, nowCube.z))
            && nowCube.x + 1 != cubeToPlace.position.x)
            positions.Add(new Vector3(nowCube.x + 1, nowCube.y, nowCube.z));
        if (IsPositionEmpty(new Vector3(nowCube.x - 1, nowCube.y, nowCube.z))
            && nowCube.x - 1 != cubeToPlace.position.x)
            positions.Add(new Vector3(nowCube.x - 1, nowCube.y, nowCube.z));
        if (IsPositionEmpty(new Vector3(nowCube.x, nowCube.y + 1, nowCube.z))
            && nowCube.y + 1 != cubeToPlace.position.y)
            positions.Add(new Vector3(nowCube.x, nowCube.y + 1, nowCube.z));
        if (IsPositionEmpty(new Vector3(nowCube.x, nowCube.y - 1, nowCube.z))
            && nowCube.y - 1 != cubeToPlace.position.y)
            positions.Add(new Vector3(nowCube.x, nowCube.y - 1, nowCube.z));
        if (IsPositionEmpty(new Vector3(nowCube.x, nowCube.y, nowCube.z + 1))
            && nowCube.z + 1 != cubeToPlace.position.z)
            positions.Add(new Vector3(nowCube.x, nowCube.y, nowCube.z + 1));
        if (IsPositionEmpty(new Vector3(nowCube.x, nowCube.y, nowCube.z - 1))
            && nowCube.z - 1 != cubeToPlace.position.z)
            positions.Add(new Vector3(nowCube.x, nowCube.y, nowCube.z - 1));
        if (positions.Count > 1) cubeToPlace.position = positions[UnityEngine.Random.Range(0, positions.Count)];
        else if (positions.Count == 0) isLose = true;
        else cubeToPlace.position = positions[0];
    }

    private bool IsPositionEmpty(Vector3 targetPos)
    {
        if (targetPos.y == 0) return false;
        foreach (Vector3 pos in allCubesPositions)
        {
            if (pos.x == targetPos.x && pos.y == targetPos.y && pos.z == targetPos.z)
                return false;
        }
        return true;
    }

    private void MoveCameraChangeBg()
    {
        int maxX = 0, maxY = 0, maxZ = 0, maxHor;
        foreach (Vector3 pos in allCubesPositions)
        {
            if (Mathf.Abs(Convert.ToInt32(pos.x)) > maxX) maxX = Convert.ToInt32(pos.x);
            if (Convert.ToInt32(pos.y) > maxY) maxY = Convert.ToInt32(pos.y);
            if (Mathf.Abs(Convert.ToInt32(pos.z)) > maxZ) maxZ = Convert.ToInt32(pos.z);
        }

        maxY--;

        if (PlayerPrefs.GetInt("score") < maxY) PlayerPrefs.SetInt("score", maxY);

        scoreTxt.text = $"<size=310><color=#A22521> best: </color></size> {PlayerPrefs.GetInt("score")} \n<size=290> now: </size> {maxY}";

        camMoveToYPosition = 5.9F + nowCube.y - 1.0F;

        maxHor = maxX > maxZ ? maxX : maxZ;
        if (maxHor % 3 == 0 && prevCountMaxHorizontal != maxHor)
        {
            mainCamera.localPosition -= new Vector3(0, 0, 2.5F);
             prevCountMaxHorizontal = maxHor;
        }

        if (maxY >= 7) toCameraColor = bgColor[2];
        else if (maxY >= 5) toCameraColor = bgColor[1];
        else if (maxY >= 3) toCameraColor = bgColor[0];
     
    }

    private void AddPossibleCubes(int till)
    {
        for(int i=0; i < till; i++)
        {
            possibleCubesToCreate.Add(cubesToCreate[i]);
        }
    }
        
}
    

struct CubePos
{
    public int x, y, z;

    public CubePos(int x,int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3 getVector()
    {
        return new Vector3(x, y, z);
    }
     public void setVector(Vector3 vector)
    {
        x = Convert.ToInt32(vector.x);
        y = Convert.ToInt32(vector.y);
        z = Convert.ToInt32(vector.z);
    }
}

