using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    private Transform cameraTransform;

    private float shakeDuration = 1.0F; // �����, ������� ������ ����� ��������
    private float shakeAmount = 0.04F; // ��������� ������ ����� �������� ������
    private float decreaseFactor = 1.5F; // ��������� ������ ����������� ������

    private Vector3 originPosition; // �������������� ������� ������

    private void Start()
    {
        cameraTransform = GetComponent<Transform>();
        originPosition = cameraTransform.localPosition;
    }

    private void Update()
    {
        if(shakeDuration > 0)                                                                         // ���� ������ - ������� ������ ��������
        {                                                                                            // (� ���� ������� ����������� ��������� �����
            cameraTransform.localPosition = originPosition + Random.insideUnitSphere * shakeAmount; //  (��������� �������� ������ ����� � �������� 1))
            shakeDuration -= Time.deltaTime * decreaseFactor; // ���������� ������
        }
        else
        {
            shakeDuration = 0;
            cameraTransform.localPosition = originPosition;
        }
        
    }
}
